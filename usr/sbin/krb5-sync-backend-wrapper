#!/bin/sh

# A wrapper around '/usr/sbin/krb5-sync-backend process' to ensure that
# only one instance of /usr/sbin/krb5-sync-backend runs at a time.

# Define the lock stuff.
# This is taken from
# https://stackoverflow.com/questions/1715137/what-is-the-best-way-to-ensure-only-one-instance-of-a-bash-script-is-running
LOCKFILE="/run/krb5-sync-backend.lock"
LOCKFD=99

_lock()             { flock -$1 $LOCKFD; }
_no_more_locking()  { _lock u; _lock xn && rm -f $LOCKFILE; }
_prepare_locking()  { eval "exec $LOCKFD>\"$LOCKFILE\""; trap _no_more_locking EXIT; }

exlock_now()        { _lock xn; }  # obtain an exclusive lock immediately or fail

_prepare_locking

exlock_now || exit 1

# If we get here, we have the lock. So, process the krb5-sync spool
# directory. We point to the spool directory specified by $1

if [ -z "$2" ]; then
    SPOOL_DIR=/var/spool/krb5-sync
else
    SPOOL_DIR=$2
fi

# FIRST: move the files. The "10" indicates that only files more than 10
# seconds old should move.

# If the LDAPSERVER environment variable is defined, use the --ldap-server
# option. This ensures we only move files when the sunetid associated
# with the file is in LDAP.

# Note that for --ldap-server option to work the KRB5CCNAME environment
# variable must be defined and be pointing at a ticket cache with
# credentials that can read the accounts tree of LDAPSERVER.

if [ -z "$LDAPSERVER" ]; then
   # Do not check LDAP for account existence.
   /usr/sbin/krb5-move-sync-files /var/spool/krb5-sync "$SPOOL_DIR" 10
else
   # DO check LDAP for account existence.
   /usr/sbin/krb5-move-sync-files --ldap-server="$LDAPSERVER" /var/spool/krb5-sync "$SPOOL_DIR" 10
fi

# SECOND: process the files.
/usr/sbin/krb5-sync-backend process -d "$SPOOL_DIR"

exit 0

# Documentation.  Use a hack to hide this from the shell.  Because of the
# above exit line, this should never be executed.
DOCS=<<__END_OF_DOCS__

=head1 NAME

krb5-sync-backend-wrapper - wrapper around krb5-sync to support locking

=head1 SYNOPSIS

B<krb5-sync-backend-wrapper> I<process> [I<spool-dir>]

=head1 DESCRIPTION

B<krb5-sync-backend-wrapper> runs a single instance of B<krb5-sync>. If
B<krb5-sync-backend-wrapper> is run while another instance of
B<krb5-sync-backend-wrapper> is already running, the second instance will
exit immediately.

If I<spool-dir> is omitted it defaults to I</var/spool/krb5-sync>.

If the environment variable B<$LDAPSERVER> is defined then we run
I</usr/sbin/krb5-move-sync-files> with the B<--ldap-server> option set to
B<$LDAPSERVER>. For the B<krb5-move-sync-files> program to be able to read
from B<$LDAPSERVER> this script must be run in the context of Kerberos
credentials that have permission to read from the server pointed to by
B<$LDAPSERVER>.

=head1 AUTHOR

Adam Lewenberg <adamhl@stanford.edu>

=head1 SEE ALSO

krb5-sync(8)

=cut

__END_OF_DOCS__
