#!/bin/bash

# 1. Number of successful authentications.
NUM_SUCC=$(grep -c "Pre-authentication succeeded" /var/log/kdc.log)
echo "Number of successful authentications: $NUM_SUCC"
echo ""

# 2. Get 10 most common IP addresses from kdc.log.
echo "Most-seen AS requests by IP address"
echo "-----------------------------------"
grep -P "AS-REQ .* from IPv4:.* for krbtgt/.*" /var/log/kdc.log \
  | cut -f 5 -d ' ' | cut -f 2 -d ':' | sort | uniq -c | sort -r | head -n 10
echo ""

# 3. Get 10 most common successful authentications by principal.
echo "Most-seen successful AS requests by principal"
echo "-----------------------------------"
grep -P "Pre-authentication succeeded" /var/log/kdc.log | cut -f 6 -d ' ' | \
  sort | uniq -c | sort -r | head -n 10
echo ""

# 4. Get 10 most common failed authentications by principal.
echo "Most-seen failed AS requests by principal"
echo "-----------------------------------"
grep -P "Decrypt integrity check failed" /var/log/kdc.log | cut -f 7 -d ' ' | \
  sort | uniq -c | sort -r | head -n 10
echo ""

exit 0

# Documentation.  Use a hack to hide this from the shell.  Because of the
# above exit line, this should never be executed.
DOCS=<<__END_OF_DOCS__

=head1 NAME

kdc-stats - display KDC usage statistics gleaned from /var/log/kdc

=head1 SYNOPSIS

kdc-stats

=head1 DESCRIPTION

B<kdc-stats> parses the file F</var/log/kdc.log> and displays the
following information:

    * number of successful authentications
    * most-seen AS requests (by IP address)
    * most-seen successful AS requests (by Kerberos principal)
    * most-seen unsuccessful AS requests (by Kerberos principal)

B<kdc-stats> is meant to run on the KDC server itself as it accessing the
file F</var/log/kdc.log>.

As this script only parses the file F</var/log/kdc.log>, the statistics
returned only go back to when that file was last rotated which is, in the
case of Stanford, usually at 11:45 PM the previous day.

=head1 OPTIONS

None.

=head1 EXIT STATUS

Returns C<0> on success, C<1> on any failure.

=head1 LICENSE AND COPYRIGHT

Copyright 2019 The Board of Trustees of the Leland Stanford Junior
University.  All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted, provided
that the above copyright notice appear in all copies and that both that
copyright notice and this permission notice appear in supporting
documentation, and that the name of Stanford University not be used in
advertising or publicity pertaining to distribution of the software
without specific, written prior permission.  Stanford University makes no
representations about the suitability of this software for any purpose.
It is provided "as is" without express or implied warranty.

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

=cut

__END_OF_DOCS__
