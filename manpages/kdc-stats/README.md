# NAME

kdc-stats - display KDC usage statistics gleaned from /var/log/kdc

# SYNOPSIS

kdc-stats

# DESCRIPTION

**kdc-stats** parses the file `/var/log/kdc.log` and displays the
following information:

    * number of sucessful authentications
    * most-seen AS requests (by IP address)
    * most-seen AS requests (by Kerberos principal)

**kdc-stats** is meant to run on the KDC server itself as it accessing the
file `/var/log/kdc.log`.

As the file `/var/log/kdc.log` you are only getting statistics back to
when that file was last rotates which is, in the case of Stanford, usually
at 11:45 PM the previous day.

# OPTIONS

None.

# EXIT STATUS

Returns `0` on success, `1` on any failure.

# LICENSE AND COPYRIGHT

Copyright 2019 The Board of Trustees of the Leland Stanford Junior
University.  All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted, provided
that the above copyright notice appear in all copies and that both that
copyright notice and this permission notice appear in supporting
documentation, and that the name of Stanford University not be used in
advertising or publicity pertaining to distribution of the software
without specific, written prior permission.  Stanford University makes no
representations about the suitability of this software for any purpose.
It is provided "as is" without express or implied warranty.

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
