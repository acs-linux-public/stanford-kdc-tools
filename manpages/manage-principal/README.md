# NAME

manage-principal - create/replace/export a kerberos service principal

# SYNOPSIS

**manage-principal** -h

**manage-principal** \[-q\] -n principal -a create (-r | -p &lt;password> | -f &lt;password-file>) \[-i\]

**manage-principal** \[-q\] -n principal -a replace (-r | -p &lt;password> | -f &lt;password-file>)

**manage-principal** \[-q\] -n principal -a export -k &lt;path\_to\_keytab>

# DESCRIPTION

**manage-principal** creates or exports a kerberos principal.

This script will only manage principals starting with "service/", "host/",
or "postgres/".

# OPTIONS

\-q: run in quiet mode, that is, don't present the "are you sure?" prompt.

# PASSWORD SPECIFICATION

In the **create** and **replace** actions, you must specify how the password
will be set.  There are three options. Use `-r` to use a random
password. Use the `-p` option when you want to provide the password on
the command line. Finally, use the `-f` option to provide the password in
a file. In this last case, the first line of the specified file is used
as the password

# EXAMPLES

## Export an existing principal

To export an existing principal:

    manage-principal -n service/testing123 -a export -k /path/to/keytab

This command will abort and exit with a non-zero code if the principal
does not exist. Note that if the keytab file already exists, then the
credentials will be _added_ to the existing keyttab file.

## Create a principal

To create a principal with a _random_ password:

    manage-principal -n service/testing123 -a create -r

To create a principal with a _specified_ password:

    manage-principal -n service/testing123 -a create -p secret

To create a principal using password file:

    manage-principal -n service/testing123 -a create -f /root/password

If the principal already exists these commands will abort and exit with a
non-zero exit code. If you want to create a principal if the principal
does not already exist, but skip the non-zero exit code if the principal
_does_ already exist, add the `-i` option:

    manage-principal -n service/testing123 -a create -i -r

## Re-create a principal

If you want to _re-create_ the principal in case it already exists, that is,
delete the old version and create a new one, use the `replace` action:

    manage-principal -nservice/testing123 -a replace -r

# AUTHOR

Adam Lewenberg <adamhl@stanford.edu>

# SEE ALSO

kadmin(1)
