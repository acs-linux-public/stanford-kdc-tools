# NAME

kdc-auth-test - test Kerberos authetnication against a KDC endpoint

# SYNOPSIS

kdc-auth-test &lt;kdc> &lt;port> &lt;keytab> &lt;realm>

# DESCRIPTION

**kdc-auth-test** performs a Kerberos authentication using the supplied keytab
file against the indicated KDC (Key Distribution Center). All four
arguments shown in the SYNOPSIS are required.

The **kdc** argumentis the hostname or IP address of the KDC you are
testing against.

The **port** argument is the port that the **kdc**'s Kerberos service
listens to (for production servers this is 88).

The **keytab** argument is the full path to a keytab file with valid
credentials that **kdc** will recognize.

The **realm** argument should be "stanford.edu".

# EXAMPLES

Example 1: test Kerberros authentication against **kdc-dev2**:

    kdc-auth-test kdc-dev2.stanford.edu 688 /tmp/test.keytab stanford.edu

Example 2: test Kerberos authentication against a production KDC:

    kdc-auth-test krb5auth1.stanford.edu 88 /tmp/test.keytab stanford.edu

# AUTHOR

Adam Lewenberg <adamhl@stanford.edu>

# SEE ALSO

k5start(1)
