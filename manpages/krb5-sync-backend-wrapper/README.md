# NAME

krb5-sync-backend-wrapper - wrapper around krb5-sync to support locking

# SYNOPSIS

krb5-sync-backend-wrapper process \[spool-dir\]

# DESCRIPTION

**krb5-sync-backend-wrapper** runs a single instance of **krb5-sync**. If
**krb5-sync-backend-wrapper** is run while another instance of
**krb5-sync-backend-wrapper** is already running, the second instance will
exit immediately.

If **spool-dir** is omitted it defaults to `/var/spool/krb5-sync`.

# AUTHOR

Adam Lewenberg <adamhl@stanford.edu>

# SEE ALSO

krb5-sync(8)
