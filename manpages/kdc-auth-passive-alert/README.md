# NAME

kdc-auth-passive-alert - test Kerberos authentication and send passive alert

# SYNOPSIS

kdc-auth-passive-alert \[-v\] &lt;kdc> &lt;port> &lt;keytab> &lt;realm> &lt;nagios-ips>

# DESCRIPTION

**kdc-auth-passive-alert** performs a Kerberos authentication using the
supplied keytab file against the indicated KDC (Key Distribution Center)
and sends the results to all of the IP addresses listed in the
comma-delimited list **nagios-ips**. All five arguments shown in the
SYNOPSIS are required.

See the man page for **kdc-auth-test** for more information.

# AUTHOR

Adam Lewenberg <adamhl@stanford.edu>

# SEE ALSO

kdc-auth-test(1)
