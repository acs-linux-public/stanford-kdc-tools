# NAME

setup-prop-principal - Set up an hprop or iprop principal on a KDC

# SYNOPSIS

**setup-prop-principal** \[-h\] \[-q\] -a &lt;create|export> -t &lt;iprop|hprop> -p &lt;password-file> -H &lt;hostname> \[-k &lt;/path/to/keytab>\]

# DESCRIPTION

**setup-prop-principal** creates and exports either an iprop or an hprop principal on a
KDC. When creating it will first delete the principal and then create it.
When exporting it exports to the keytab file /etc/heimdal-kdc/prop.keytab.

Both the **password-file** and **hostname** arguments must be given.

The first line of the password file will be interpreted as the password to
use for the principal.

# AUTHOR

Adam Lewenberg <adamhl@stanford.edu>

# SEE ALSO

kadmin(1)
